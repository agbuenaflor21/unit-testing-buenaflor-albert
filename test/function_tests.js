//IMPORT DEPENDENCY
const {expect} = require('chai');

// IMPORT FUNCTIONS
const {exchange, exchangeRates} = require('./../src/utils.js');

// TEST
// it('text_exchange_usd_to_php', ()=>{
// 	const phpValue = exchange('usd', 'peso', 1000);
// 	expect(phpValue).to.equal(50730);
// });

// ACTIVITY
// it('text_exchange_usd_to_yen', ()=>{
// 	const yenValue = exchange('usd', 'yen', 1000);
// 	expect(yenValue).to.equal(108630);
// });

// it('text_exchange_usd_to_yuan', ()=>{
// 	const yuanValue = exchange('usd', 'yuan', 1000);
// 	expect(yuanValue).to.equal(7030);
// });

// it('text_exchange_usd_to_won', ()=>{
// 	const wonValue = exchange('usd', 'won', 1000);
// 	expect(wonValue).to.equal(1187240);
// });

// TEST SUITE DEMO

// we are trying to learn how to write and run tests for our js programs using Mocha
// -> why do we do unit testing?
// -> build a software that is predictable 
// -> less error-prone and resilient to changes
// TDD( Test-Driven Development)
// -> is one of the major ways to achieve a much more efficient way of software dev.
// TDD usually requires the following:
// -> writing tests for the required software functionality
// -> run the tests for the software functionality.
// -> implement the software functionality 
// -> fix bugs and refactor the code until all the test pass the requirements
// -> Repeat the cycle for new functionalities.


// mocha -> unit testing framework -
// 		 -> is a JS test runner that runs both on Node.js and in the browser
//		 -> it provides functionality for testing both synchronous and asynchronous code with a very simple and similar interface.


// SHORT GUIDE IN INSTALLING MOCHA:
// -> here is how we install Mocha using node package manager(npm)
// Global installation:
// *npm i --global mocha*
// Development dependency for your project
// *npm i --save-dev mocha*


// describe() is not part of js, but rather it is a function defined in  the library that we used (mocha)
// -> is a function in the Jasmine Testing Framework. It simply describes the suite of test cases enumerated by the 'it' functions.
// parang Aliasing kumbaga - we tried to put similar functions in one family


// Assertion LIbrary?
// -> if you are using Mocha in a Node.js Environment, you can use the built-in assert module as our assertion library.
// -> if we want a more extensive assertion library we can make use of either of the following:
// 		-> Chai
// 		-> Expect.js
// 		-> Should.js

// INSTALLATION SYNTAX FOR CHAI VIA NPM:
// *npm i --save-dev chai*

// if you install mocha & chai altogether:
// *npm i chat chai-http mocha*
// chai -> assertion library
// chai-http -> request handler
// mocha -> unit testing framework

// What does Chai provides? 
// -> Chai provides the following assertion styles
// 		-> Assert Style
// 		-> Expect Style
// 		-> Should Style

// Javascript has several tools and frameworks that makes unit testing or TDD possible on node.js and also in browser.
// -> Mocha 
// -> Jasmine
// -> Jest
// -> Karma
// -> Cypress

// describe('test_exchange_usd_to_multiple_currencies', ()=> {
// 	// convert usd to php
// 	it('text_exchange_usd_to_php', ()=> {
// 		const phpValue = exchange('usd', 'peso', 1000);
// 		expect(phpValue).to.equal(50730);
// 	});

// 	//convert usd to yen
// 	it('text_exchange_usd_to_yen', ()=>{
// 		const phpValue = exchange('usd', 'yen', 1000);
// 		expect(phpValue).to.equal(108630);
// 	});

// 	//convert usd to yuan
// 	it('text_exchange_usd_to_yuan', ()=>{
// 		const phpValue = exchange('usd', 'yuan', 1000);
// 		expect(phpValue).to.equal(7030);
// 	});
// 		//convert usd to won
// 	it('test_exchange_usd_to_won', ()=>{
// 		const phpValue = exchange('usd', 'won', 1000);
// 		expect(phpValue).to.equal(1187240)
// 	});
// })

// Activity 2

describe('test_exchange_php_to_multiple_currencies', ()=> {
	// convert php to usd
	it('text_exchange_php_to_usd', ()=> {
		const phpValue = exchange('peso', 'usd', 1000);
		expect(phpValue).to.equal(20);
	});

	//convert php to yen
	it('text_exchange_php_to_yen', ()=>{
		const phpValue = exchange('peso', 'yen', 1000);
		expect(phpValue).to.equal(2140);
	});

	//convert php to yuan
	it('text_exchange_php_to_yuan', ()=>{
		const phpValue = exchange('peso', 'yuan', 1000);
		expect(phpValue).to.equal(140);
	});
	//convert php to won
	it('test_exchange_php_to_won', ()=>{
		const phpValue = exchange('peso', 'won', 1000);
		expect(phpValue).to.equal(23390)
	});
})