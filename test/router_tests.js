const chai = require('chai'); // assertion library
const {expect} = chai; // style
const chaiHttp = require('chai-http'); // request
chai.use(chaiHttp);

//SERVER URL
const serverUrl = 'http://localhost:3000';

// TEST SUITE
describe('GET ALL RATES', ()=> {
	it('should accept http requests', ()=>{
		chai
		.request(serverUrl)
		.get('/rates')
		.end((err, res)=> expect(res).to.not.equal(undefined))

	})
	it('should have a status of 200', ()=>{
		chai
		.request(serverUrl)
		.get('/rates')
		.end((err, res) => {
			expect(res).to.have.status(200)
			
		})
	})
	it('should return an object w/ a property of rates', ()=> {
		chai
		.request(serverUrl)
		.get('/rates')
		.end((err, res)=> {
			expect(res.body).to.have.property('rates')
			
		})
	})
	it('should have an object w/ a property of rates w/ the length of 5', ()=>{
		chai
		.request(serverUrl)
		.get('/rates')
		.end((err, res) => {
			expect(Object.keys(res.body.rates)).lengthOf(5)
		
			
		})
	})
})